import UIKit
import Firebase
import CoreLocation

extension UsersController : CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[locations.count-1]
        if location.horizontalAccuracy > 0 {
            
            let longitude = String(location.coordinate.longitude)
            let latitude = String(location.coordinate.latitude)
            let coordinates = ["lat" : latitude, "lon" : longitude]
            uploadLocationData(locationData: coordinates)
        }
        
        
    }
    
    func uploadLocationData(locationData: [String : String]) {
        
        guard let uid = Auth.auth().currentUser?.uid else {
            print("uploadLocationData ERROR: NO UID FOUND")
            return
        }
        
        let ref = Database.database().reference().child("location").child(uid)
        ref.updateChildValues(locationData)
        
    }
}
