//
//  ViewController.swift
//  buddyFinder
//
//  Created by Benjamin Mecanovic on 25/01/2019.
//  Copyright © 2019 hydro1337x. All rights reserved.
//

import UIKit
import Firebase
import CoreLocation
import SVProgressHUD

class UsersController: UITableViewController, FetchUsersDelegate{
    

    let cellID = "cellID"
    var users = [User]()         // array of users
    var loginController: LoginController?
    var locationManager: CLLocationManager?
    var mapViewController: MapViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loginController = LoginController()
        loginController?.fetchUsersDelegate = self
        loginController?.usersController = self
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.desiredAccuracy = kCLLocationAccuracyBest
        locationManager?.requestWhenInUseAuthorization()
        mapViewController = MapViewController()
        mapViewController?.usersController = self
        
        tableView.register(UserCell.self, forCellReuseIdentifier: cellID)
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Logout", style: .plain, target: self, action: #selector(handleLogout))
        checkIfUserIsLoggedIn() //i will need to call this every time a user logs in
       
        
    }
    
    func transferFetchUsers() {
        fetchUsers()
    }
    
    func fetchUsers(){
        
        Database.database().reference().child("users").observe(.childAdded, with: { (DataSnapshot) in
            print(DataSnapshot)
            if let dictionary = DataSnapshot.value as? [String: AnyObject]{
                let user = User()
                user.name = dictionary["name"] as? String
                user.email = dictionary["email"] as? String
                user.profileImageUrl = dictionary["profileImageUrl"] as? String
                user.id = dictionary["id"] as? String
                self.users.append(user)

                // if we call just tableView.reloadData the app will crash, insead we need to call it inside
                // DispatchQueue.main.async
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                
            }
            
        }, withCancel: nil)
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! UserCell
        let user = users[indexPath.row]
        cell.textLabel?.text = user.name
        cell.detailTextLabel?.text = user.email
        
        if let imageUrl = user.profileImageUrl {
            
            cell.profileImageView.loadImageUsingCacheWithURLString(urlString: imageUrl)
            
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let uid = users[indexPath.row].id, let name = users[indexPath.row].name else {
            return
        }
        let ref = Database.database().reference().child("users").child(uid).child("isOnline")
        ref.observeSingleEvent(of: .value) { (snapshot) in
            if let isOnline = snapshot.value as? String {
                if isOnline == "true" {
                    self.mapViewController?.observeUserLocationData(userID: uid)
                    self.navigationController?.pushViewController(self.mapViewController!, animated: true)
                }
                else {
                    
                    print("User is offline")
                    SVProgressHUD.showError(withStatus: "Whoops!\n\(name) is currently offline.")
                    tableView.cellForRow(at: indexPath)?.isSelected = false
                }
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72
    }
    
    
    func emptyTableView(){
        
        users.removeAll(keepingCapacity: false)
        self.tableView.reloadData()
        
    }
    
    func checkIfUserIsLoggedIn(){
        // if user is not logged in
        if Auth.auth().currentUser?.uid == nil{
            perform(#selector(handleLogout), with: nil, afterDelay: 0)  // silences warrning for loading multiple controllers at the app start
        } else {
            locationManager?.startUpdatingLocation()
            setupNavigationTitle()
            self.fetchUsers()
        }
    }
    
    func setupNavigationTitle(){
        // get user info if logged in
        let uid = Auth.auth().currentUser?.uid
        Database.database().reference().child("users").child(uid!).observeSingleEvent(of: .value
            , with: { (DataSnapshot) in
                
                if let dictionary = DataSnapshot.value as? [String: AnyObject] {
                    
                    self.navigationItem.title = dictionary["name"] as? String
                    
                }
                
        }, withCancel: nil)
    }
    
    // uppon logout it jumps from ViewController to LoginController
    @objc func handleLogout(){
        locationManager?.stopUpdatingLocation()
        changeIsOnlineToFalse()
        do{
            try Auth.auth().signOut()
        } catch let logoutError{
            print(logoutError)
        }
        loginController?.clearLoginRegisterInputs()
        emptyTableView()
        present(loginController!, animated: true, completion: nil)
        
    }
    
    func changeIsOnlineToFalse(){
        
        guard let uid = Auth.auth().currentUser?.uid else {
            return
        }
        
        let ref = Database.database().reference()
        let usersReference = ref.child("users").child(uid).child("isOnline")
        usersReference.setValue("false")
        
    }
}

