
import UIKit
import MapKit
import Firebase

class MapViewController: UIViewController {
    var mapView: MKMapView?
    var firebaseObserver: DatabaseHandle?
    var ref: DatabaseReference?
    var usersController: UsersController?
    let dropPin = MKPointAnnotation()
    var flag = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        usersController = UsersController()
        usersController?.mapViewController = self
        view.backgroundColor = UIColor.white
        mapView = MKMapView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height))
        view.addSubview(mapView!)
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(dismissMapView))
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        ref?.removeAllObservers()
    }
    
    @objc func dismissMapView() {
        flag = 0
        navigationController?.popViewController(animated: true)
    }
    
    func observeUserLocationData (userID: String) {
        
        ref = Database.database().reference().child("location").child(userID)
        firebaseObserver = ref?.observe(.value, with: { (snapshot) in
            
            if let locationData = snapshot.value as? [String : AnyObject]{
                if self.flag == 1 {
                    print("IF")
                    let latitude = (locationData["lat"] as! NSString).doubleValue
                    let longitude = (locationData["lon"] as! NSString).doubleValue
                    let center = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                    UIView.animate(withDuration: 1, animations: {
                        self.dropPin.coordinate = center
                    })
                } else {
                    print("ELSE")
                    let latitude = (locationData["lat"] as! NSString).doubleValue
                    let longitude = (locationData["lon"] as! NSString).doubleValue
                    let center = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                    let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
                    self.dropPin.coordinate = center
                    self.mapView?.setRegion(region, animated: true)
                    self.mapView?.addAnnotation(self.dropPin)
                    self.flag = 1
                }
            }
        })

    }
    
}
