//
//  LoginControllerViewController.swift
//  buddyFinder
//
//  Created by Benjamin Mecanovic on 25/01/2019.
//  Copyright © 2019 hydro1337x. All rights reserved.
//

import UIKit
import Firebase
import Photos
import SVProgressHUD



class LoginController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var fetchUsersDelegate: FetchUsersDelegate?
    var usersController: UsersController?
    
    let inputsContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.translatesAutoresizingMaskIntoConstraints = false //enables programatic View Constraints
        view.layer.cornerRadius = 5
        view.layer.masksToBounds = true //corner radius wont work without settings this to "true"
        return view
    }() //makes viewDidLoad cleaner
    
    lazy var loginRegisterButton: UIButton = {       //lazy var to access self inside the function
        let button = UIButton(type: .system)
        button.backgroundColor = UIColor.white
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Register", for: .normal)
        button.setTitleColor(UIColor(r: 238, g: 106, b: 80), for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        button.layer.cornerRadius = 25
        button.layer.masksToBounds = true
        button.addTarget(self, action: #selector(handleLoginRegister), for: .touchUpInside)
        
        return button
    }()
    
    
    
    @objc func handleLoginRegister(){
        
        if loginRegisterSegmentedControl.selectedSegmentIndex == 0 {
            handleLogin()
        } else {
            handleRegister()
        }
    }
    
    func handleLogin() {
        
        guard let email = emailTextField.text, let password = passwordTextField.text else {
            print("Registration form is not valid")
            return
        }
        SVProgressHUD.show()
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            
            if error != nil {
                print(error!)
                SVProgressHUD.dismiss()
                SVProgressHUD.showError(withStatus: "Login failed!\nCheck your information.")
                return
            }
            print("Sucessfully logged in")
            self.changeIsOnlineToTrue()
            self.fetchUsersDelegate?.transferFetchUsers()
            self.usersController?.setupNavigationTitle()
            self.usersController?.locationManager?.startUpdatingLocation()
            SVProgressHUD.dismiss()
            self.dismiss(animated: true, completion: nil)
            
        }
        
    }
    
    func handleRegister(){
        
        
        guard let email = emailTextField.text, let password = passwordTextField.text, let name = nameTextField.text else {
            print("Registration form is not valid")
            return
        }
        SVProgressHUD.show()
        Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
            if error != nil {
                print(error!)
                SVProgressHUD.dismiss()
                SVProgressHUD.showError(withStatus: "Registration failed!\nCheck your information.")
                return
            }
            guard let uid = user?.user.uid else {
                return
            }
            //Sucessfully authenticated
            let imageName = NSUUID().uuidString
            let storageRef = Storage.storage().reference().child("\(imageName).png")
            
            if let uploadData = self.profileImageView.image?.jpegData(compressionQuality: 0.1) {
                
                storageRef.putData(uploadData, metadata: nil, completion: { (metadata, error) in
                    
                    if error != nil{
                        print(error!)
                        SVProgressHUD.dismiss()
                        SVProgressHUD.showError(withStatus: "Image upload failed!")
                        return
                    }
                    
                    storageRef.downloadURL(completion: { (url, err) in
                        if err != nil {
                            SVProgressHUD.dismiss()
                            SVProgressHUD.showError(withStatus: "Failed getting Image URL")
                            print(err!)
                            return
                        }
                        if let profileImageUrl = url {
                            let values = ["name" : name, "email" : email, "profileImageUrl" : profileImageUrl.absoluteString, "isOnline" : "true", "id" : uid]
                            self.registerUserIntoDatabaseWithUID(uid: uid, values: values)
                        }
                    })
                })
            }
        }
    }
    
    
    private func registerUserIntoDatabaseWithUID(uid: String, values: [String: String]){
        
        let ref = Database.database().reference()
        let usersReference = ref.child("users").child(uid)
        
        usersReference.updateChildValues(values, withCompletionBlock: { (err, ref) in
            
            if err != nil{
                print(err!)
                SVProgressHUD.dismiss()
                SVProgressHUD.showError(withStatus: "Failed uploading user data!")
                return
            }
            
            print("Sucessfully registered user into Firebase Database")
            self.fetchUsersDelegate?.transferFetchUsers()
            self.usersController?.locationManager?.startUpdatingLocation()
            self.usersController?.navigationItem.title = values["name"]
            SVProgressHUD.dismiss()
            self.dismiss(animated: true, completion: nil)
            
        })
    }
    
    let applicationNameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "GraniteRegular", size: 30)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.sizeToFit()
        label.textAlignment = .center
        label.textColor = UIColor.white
        label.shadowColor = UIColor.black
        label.shadowOffset = CGSize(width: 1, height: 1)
        label.text = "buddyFinder"
        return label
    }()
    
    let underlineImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "underline")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        return imageView
    }()
    
    let nameTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "Name"
        tf.translatesAutoresizingMaskIntoConstraints = false
        return tf
    }()
    
    let nameSeparatorView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(r: 220, g: 220, b: 220)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let emailTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "Email"
        tf.translatesAutoresizingMaskIntoConstraints = false
        return tf
    }()
    
    let emailSeparatorView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(r: 220, g: 220, b: 220)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let passwordTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "Password"
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.isSecureTextEntry = true
        return tf
    }()
    
    lazy var profileImageView: UIImageView = {
       let imageView = UIImageView()
        imageView.image = UIImage(named: "profileImage")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleSelectProfileImageView)))
        imageView.isUserInteractionEnabled = true
        
        return imageView
    }()
    
    let profilePictureContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false //enables programatic View Constraints
        view.layer.cornerRadius = 5
        view.layer.masksToBounds = true //corner radius wont work without settings this to "true"
        return view
    }()
    
    @objc func handleSelectProfileImageView() {
        
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = true
        present(picker, animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        var selectedImageFromPicker: UIImage?
        
        if let editedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage{
            selectedImageFromPicker = editedImage
        }
        else if let originalImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            
            selectedImageFromPicker = originalImage
            
        }
        
        if let selectedImage = selectedImageFromPicker {
            profileImageView.image = selectedImage
        }
        dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("canceled picking")
        dismiss(animated: true, completion: nil)
    }
    
    lazy var loginRegisterSegmentedControl: UISegmentedControl = {
        let sc = UISegmentedControl(items: ["Login", "Register"])
        sc.translatesAutoresizingMaskIntoConstraints = false
        sc.tintColor = UIColor.white
        sc.selectedSegmentIndex = 1
        sc.addTarget(self, action: #selector(handleLoginRegisterChange), for: .valueChanged)
        return sc
    }()
    
    @objc func handleLoginRegisterChange(){
        let title = loginRegisterSegmentedControl.titleForSegment(at: loginRegisterSegmentedControl.selectedSegmentIndex)
        loginRegisterButton.setTitle(title, for: .normal)
        clearLoginRegisterInputs()
        
        //change height of inputsContainerView
        inputsContainerViewHeightAnchor?.constant = loginRegisterSegmentedControl.selectedSegmentIndex == 0 ? 100 : 150
        loginRegisterSegmentedControl.selectedSegmentIndex == 0 ? animateBackgroundEaseIn() : animateBackgroundEaseOut()
        //change height of nameTextField
        nameTextFieldHeightAnchor?.isActive = false
        nameTextFieldHeightAnchor = nameTextField.heightAnchor.constraint(equalTo: inputsContainerView.heightAnchor, multiplier: loginRegisterSegmentedControl.selectedSegmentIndex == 0 ? 0 : 1/3)
        nameTextFieldHeightAnchor?.isActive = true
        emailTextFieldHeightAnchor?.isActive = false
        emailTextFieldHeightAnchor = emailTextField.heightAnchor.constraint(equalTo: inputsContainerView.heightAnchor, multiplier: loginRegisterSegmentedControl.selectedSegmentIndex == 0 ? 1/2 : 1/3)
        emailTextFieldHeightAnchor?.isActive = true
        passwordTextFieldHeightAnchor?.isActive = false
        passwordTextFieldHeightAnchor = passwordTextField.heightAnchor.constraint(equalTo: inputsContainerView.heightAnchor, multiplier: loginRegisterSegmentedControl.selectedSegmentIndex == 0 ? 1/2 : 1/3)
        passwordTextFieldHeightAnchor?.isActive = true
        profileImageViewHeightAnchor?.constant = loginRegisterSegmentedControl.selectedSegmentIndex == 0 ? 0 : 150
        profileImageViewWidthAnchor?.constant = loginRegisterSegmentedControl.selectedSegmentIndex == 0 ? 0 : 150
        animateViews()
        
    }
    
    func setupToolbarDoneButton(textField: UITextField){
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(removeKeyboard))
        toolbar.setItems([flexibleSpace,doneButton], animated: true)
        textField.inputAccessoryView = toolbar
        
    }
    
    @objc func removeKeyboard(){
        view.endEditing(true)
    }
    
    let backgroundGradient = CAGradientLayer()
    let loginRegisterButtonGradient = CAGradientLayer()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor(r:61, g:91, b:151)

        setupGradientBackground()
        
        view.addSubview(profilePictureContainerView)
        view.addSubview(inputsContainerView)
        view.addSubview(loginRegisterButton)
        view.addSubview(underlineImageView)
        view.addSubview(applicationNameLabel)
        view.addSubview(loginRegisterSegmentedControl)
        
        
        
        setupProfileImageContainerView()
        setupInputsContainerView()
        setupLoginRegisterButton()
        setupProfileImageContainerView()
        setupLoginRegisterSegmentedControl()
        setupApplicationNameLabel()
        setupUnderlineImageView()
        
        setupToolbarDoneButton(textField: nameTextField)
        setupToolbarDoneButton(textField: emailTextField)
        setupToolbarDoneButton(textField: passwordTextField)
        
        setupCustomSVProgressHUD()
        
        allowPhotoAcess()
    }
    
    func setupLoginRegisterSegmentedControl(){
        //need x,y,width,heigth constraints
        loginRegisterSegmentedControl.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        loginRegisterSegmentedControl.bottomAnchor.constraint(equalTo: inputsContainerView.topAnchor, constant: -12).isActive = true
        loginRegisterSegmentedControl.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        loginRegisterSegmentedControl.heightAnchor.constraint(equalToConstant: 36).isActive = true
    }
    
    var inputsContainerViewHeightAnchor: NSLayoutConstraint?
    var nameTextFieldHeightAnchor: NSLayoutConstraint?
    var emailTextFieldHeightAnchor: NSLayoutConstraint?
    var passwordTextFieldHeightAnchor: NSLayoutConstraint?
    
    func setupInputsContainerView(){
        //need x,y,width,heigth constraints
        inputsContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        inputsContainerView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        inputsContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        inputsContainerViewHeightAnchor = inputsContainerView.heightAnchor.constraint(equalToConstant: 150)
        inputsContainerViewHeightAnchor?.isActive = true
        
        inputsContainerView.addSubview(nameTextField)
        inputsContainerView.addSubview(nameSeparatorView)
        inputsContainerView.addSubview(emailTextField)
        inputsContainerView.addSubview(emailSeparatorView)
        inputsContainerView.addSubview(passwordTextField)
        
        
        //need x,y,width,heigth constraints
        nameTextField.leftAnchor.constraint(equalTo: inputsContainerView.leftAnchor, constant: 12).isActive = true
        nameTextField.topAnchor.constraint(equalTo: inputsContainerView.topAnchor).isActive = true
        nameTextField.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        nameTextFieldHeightAnchor = nameTextField.heightAnchor.constraint(equalTo: inputsContainerView.heightAnchor, multiplier: 1/3)
        nameTextFieldHeightAnchor?.isActive = true
        
        //need x,y,width,heigth constraints
        nameSeparatorView.leftAnchor.constraint(equalTo: inputsContainerView.leftAnchor).isActive = true
        nameSeparatorView.topAnchor.constraint(equalTo: nameTextField.bottomAnchor).isActive = true
        nameSeparatorView.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        nameSeparatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        //need x,y,width,heigth constraints
        emailTextField.leftAnchor.constraint(equalTo: inputsContainerView.leftAnchor, constant: 12).isActive = true
        emailTextField.topAnchor.constraint(equalTo: nameTextField.bottomAnchor).isActive = true
        emailTextField.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        emailTextFieldHeightAnchor = emailTextField.heightAnchor.constraint(equalTo: inputsContainerView.heightAnchor, multiplier: 1/3)
        emailTextFieldHeightAnchor?.isActive = true
        
        //need x,y,width,heigth constraints
        emailSeparatorView.leftAnchor.constraint(equalTo: inputsContainerView.leftAnchor).isActive = true
        emailSeparatorView.topAnchor.constraint(equalTo: emailTextField.bottomAnchor).isActive = true
        emailSeparatorView.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        emailSeparatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        //need x,y,width,heigth constraints
        passwordTextField.leftAnchor.constraint(equalTo: inputsContainerView.leftAnchor, constant: 12).isActive = true
        passwordTextField.topAnchor.constraint(equalTo: emailTextField.bottomAnchor).isActive = true
        passwordTextField.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        passwordTextFieldHeightAnchor = passwordTextField.heightAnchor.constraint(equalTo: inputsContainerView.heightAnchor, multiplier: 1/3)
        passwordTextFieldHeightAnchor?.isActive = true
        
    }
    
    func setupLoginRegisterButton(){
        //need x,y,width,heigth constraints
        loginRegisterButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        loginRegisterButton.topAnchor.constraint(equalTo: inputsContainerView.bottomAnchor, constant: 12).isActive = true
        loginRegisterButton.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        loginRegisterButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    var profileImageViewWidthAnchor: NSLayoutConstraint?
    var profileImageViewHeightAnchor: NSLayoutConstraint?
    
    func setupProfileImageContainerView() {
        
        profilePictureContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        profilePictureContainerView.bottomAnchor.constraint(equalTo: loginRegisterSegmentedControl.topAnchor, constant: -12).isActive = true
        profilePictureContainerView.widthAnchor.constraint(equalToConstant: 150).isActive = true
        profilePictureContainerView.heightAnchor.constraint(equalToConstant: 150).isActive = true
        profilePictureContainerView.addSubview(profileImageView)
        
        profileImageView.centerXAnchor.constraint(equalTo: profilePictureContainerView.centerXAnchor).isActive = true
        profileImageView.centerYAnchor.constraint(equalTo: profilePictureContainerView.centerYAnchor).isActive = true
        profileImageViewWidthAnchor = profileImageView.widthAnchor.constraint(equalToConstant: 150)
        profileImageViewWidthAnchor?.isActive = true
        profileImageViewHeightAnchor = profileImageView.heightAnchor.constraint(equalToConstant: 150)
        profileImageViewHeightAnchor?.isActive = true

    }
    
    func setupApplicationNameLabel() {
        applicationNameLabel.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -50).isActive = true
        applicationNameLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        applicationNameLabel.widthAnchor.constraint(equalToConstant: 173).isActive = true
        applicationNameLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    func setupUnderlineImageView() {
        underlineImageView.topAnchor.constraint(equalTo: applicationNameLabel.bottomAnchor, constant: -25).isActive = true
        underlineImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        underlineImageView.widthAnchor.constraint(equalToConstant: 170).isActive = true
        underlineImageView.heightAnchor.constraint(equalToConstant: 30).isActive = true
    }
    
    func clearLoginRegisterInputs() {
        nameTextField.text = ""
        emailTextField.text = ""
        passwordTextField.text = ""
        profileImageView.image = UIImage(named: "profileImage")
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func animateViews(){
        UIView.animate(withDuration: 0.35) {
            self.view.layoutIfNeeded()
        }
    }
    
    func setupGradientBackground() {
        backgroundGradient.frame = view.bounds
        backgroundGradient.colors = [
            UIColor(red: 48/255, green: 62/255, blue: 103/255, alpha: 1).cgColor,
            UIColor(red: 244/255, green: 88/255, blue: 53/255, alpha: 1).cgColor
        ]
        backgroundGradient.locations = [0.0, 1.0]
        backgroundGradient.startPoint = CGPoint(x:0, y:0)
        backgroundGradient.endPoint = CGPoint(x:1, y:1)
        view.layer.insertSublayer(backgroundGradient, at: 0)
    }
    
    func animateBackgroundEaseIn() {
        let gradientChangeAnimation = CABasicAnimation(keyPath: "colors")
        gradientChangeAnimation.duration = 1
        gradientChangeAnimation.toValue = [
            UIColor(red: 244/255, green: 88/255, blue: 53/255, alpha: 1).cgColor,
            UIColor(red: 196/255, green: 70/255, blue: 107/255, alpha: 1).cgColor
        ]
        gradientChangeAnimation.fillMode = CAMediaTimingFillMode.forwards
        gradientChangeAnimation.isRemovedOnCompletion = false
        backgroundGradient.add(gradientChangeAnimation, forKey: "colorChange")
    }
    
    func animateBackgroundEaseOut() {
        let gradientChangeAnimation = CABasicAnimation(keyPath: "colors")
        gradientChangeAnimation.duration = 1
        gradientChangeAnimation.toValue = [
            UIColor(red: 48/255, green: 62/255, blue: 103/255, alpha: 1).cgColor,
            UIColor(red: 244/255, green: 88/255, blue: 53/255, alpha: 1).cgColor
        ]
        gradientChangeAnimation.fillMode = CAMediaTimingFillMode.forwards
        gradientChangeAnimation.isRemovedOnCompletion = false
        backgroundGradient.add(gradientChangeAnimation, forKey: "colorChange1")
    }
    
    func allowPhotoAcess(){
        if PHPhotoLibrary.authorizationStatus() != PHAuthorizationStatus.authorized {
            PHPhotoLibrary.requestAuthorization({ (status: PHAuthorizationStatus) in
                
            })
        }
    }
    
    func changeIsOnlineToTrue(){
        
        guard let uid = Auth.auth().currentUser?.uid else {
            return
        }
        
        let ref = Database.database().reference()
        let usersReference = ref.child("users").child(uid).child("isOnline")
        usersReference.setValue("true")
        
    }
    
    func setupCustomSVProgressHUD() {
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.setForegroundColor(UIColor(r: 238, g: 106, b: 80))
        SVProgressHUD.setMinimumDismissTimeInterval(0.8)
        SVProgressHUD.setMaximumDismissTimeInterval(2.0)
    }
}

extension UIColor {
    
    convenience init (r: CGFloat, g: CGFloat, b: CGFloat){
        self.init(red: r/255, green: g/255, blue: b/255, alpha: 1)  //convinience constructor created just to get rid of redundant typing of "255"
    }
}
