//
//  User.swift
//  buddyFinder
//
//  Created by Benjamin Mecanovic on 26/01/2019.
//  Copyright © 2019 hydro1337x. All rights reserved.
//

import UIKit

class User: NSObject {
    
    var name: String?
    var email: String?
    var profileImageUrl: String?
    var id: String?

}
